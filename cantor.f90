module MYDATA_MODULE

type MYDATA
    character(len=20) :: string
end type MYDATA

end module

module MYDATA_LISTS
    use MYDATA_MODULE, LIST_DATA => MYDATA
    include "linkedlist.f90"
end module MYDATA_LISTS


program CantorCircularProces
    use MYDATA_LISTS
    implicit none
    real :: theta,num_of_cantors,circle_leanght,sinus_alpha,sinus_beta, selected_radius, replacment_angle
    integer :: cantor_step,n,m,radius
    type(LINKED_LIST), pointer :: list
    type(LINKED_LIST), pointer :: head
    real, dimension(:,:), allocatable :: alpha
    real, dimension(:,:), allocatable :: beta
    real, parameter:: pi=3.1415926
    common /coeff/ cantor_step
    

    cantor_step = 3
    radius = 30
    

    theta = (299792458/(1.75*(10**9)))/2
    
    theta = theta * 100
    
    num_of_cantors =2*radius*pi
    num_of_cantors = num_of_cantors- MODULO(num_of_cantors,theta)
    num_of_cantors = (num_of_cantors/theta)+1
    
    
    
    
    

    call CantorSet(0,90,1,list)
    
    allocate(alpha(1:2**cantor_step))
    
    allocate(beta(2**cantor_step:num_of_cantors))
    
    do n=1,2**cantor_step
        alpha(1:n) = CALL list_get_data(list)
        head = list
	call list_delete_element(list,head)
    end do
    
    do n=1,2**cantor_step
        sinus_alpha=sin(alpha(1:n))
        if (sinus_alpha < 0) then
            sinus_alpha = -sinus_alpha
        end if
        
        selected_radius = radius * sinus_alpha
        circle_leanght = 2 * selected_radius * pi
        num_of_cantors = circle_leanght/theta
	replacment_angle= circle_leanght/int(num_of_cantors)
        do m=1,int(num_of_cantors)
            beta(n)(m) = ((m*replacment_angle*360)/(2*pi*selected_radius)
        end do    
    end do
    
    call reset()
    
end program CantorCircularProces




RECURSIVE SUBROUTINE CantorSet(dg, gg, i,list)
    use MYDATA_LISTS
    integer, intent(in)::i
    type(LINKED_LIST), pointer :: list
    type(LINKED_LIST), pointer :: next
    real, intent(in) :: dg,gg
    real             :: ndg, ngg, cantor_step
    common /coeff/ cantor_step
    
    if(i==cantor_step) then
        if(list_count(list)==null) then
                call list_create(list,gg)
        else
                next = list%next
                DO WHILE ( next /= null )
                    next = list%next
                END DO
                call list_insert(next,gg)
        end if
    else
        ndg=dg+(gg-dg)*(2/3)
        ngg=dg+(gg-dg)*(1/3)
        
        CALL CantorSet(dg,ngg,i+1,list)
        CALL CantorSet(ndg,gg,i+1,list)
    end if
    
end subroutine
